import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/bloc/homepage_bloc.dart';
import 'package:weather_app/constants.dart';
import 'package:weather_app/events/homepage_event.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/responsitory/user_res.dart';
import 'package:weather_app/services/navigation_services.dart';
import 'package:weather_app/views/authentication_page.dart';
import 'package:weather_app/views/home_page.dart';
import 'package:weather_app/views/register_page.dart';
import 'package:weather_app/views/splash_page.dart';
import 'bloc/authentication_bloc.dart';
import 'bloc/register_bloc.dart';
import 'models/user.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setup();
  User _user = await UserResponse.loadUserFromLocal();
  if(_user.email == "" ||
      _user.password == "" ||
      _user.token == "") {
    runApp(const WeatherApp(loadingType: Constants.loadingTypeLogin));
  }
  else {
    injection.registerSingleton<User>(_user);
    runApp(const WeatherApp(loadingType: Constants.loadingTypeHomePage));
  }
}

class WeatherApp extends StatelessWidget {
  const WeatherApp({Key? key, required this.loadingType}) : super(key: key);

  final int loadingType;

  @override
  Widget build(BuildContext context) {
    ThemeData _themeData = ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.blue[50]!,
      textTheme: const TextTheme(
        headline1: TextStyle(
          fontFamily: "HelveticaNeueBold",
          fontSize: 13,
          color: Colors.red,
        ),
        subtitle1: TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontFamily: "HelveticaNeueBold",
        ),
        subtitle2: TextStyle(
          fontFamily: "HelveticaNeueBold",
          fontSize: 15,
          color: Colors.black,
        ),
      )


    );
    String _initialRoute = "";
    switch(loadingType) {
      case Constants.loadingTypeLogin:
        _initialRoute = "/splash";
        break;
      case Constants.loadingTypeHomePage:
        _initialRoute = "/home";
        break;
    }
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => injection<AuthenticationBloc>()),
          BlocProvider(create: (context) => injection<RegisterBloc>()),
          BlocProvider(create: (context) => injection<HomePageBloc>()..add(HomePageEventLoadingCity()))
        ],
        child: MaterialApp(
          navigatorKey: injection<NavigationService>().navigatorKey,
          theme: _themeData,
          debugShowCheckedModeBanner: false,
          initialRoute: _initialRoute,
          routes: {
            "/Login": (context) => const AuthenticationPage(),
            "/signUp": (context) => const RegisterPage(),
            "/home": (context) => const HomePage(),
            "/splash": (context) => const SplashPage()
          },
        )
    );
  }
}

