

abstract class HomePageEvent {}

class HomePageEventLoadingCity extends HomePageEvent{}

class HomePageEventSearching extends HomePageEvent {
  String city;
  HomePageEventSearching({required this.city});
}
