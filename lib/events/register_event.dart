
abstract class RegisterEvent{}

class RegisterEventSubmitting extends RegisterEvent {
  String email;
  String password;
  String passwordConfirm;
  String firstName;
  String lastName;

  RegisterEventSubmitting({
    required this.email,
    required this.password,
    required this.passwordConfirm,
    required this.firstName,
    required this.lastName});
}



