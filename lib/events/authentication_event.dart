
abstract class AuthenticationEvent {}

class AuthenticationEventSubmit extends AuthenticationEvent {
  String email;
  String password;

  AuthenticationEventSubmit({required this.email, required this.password});
}

class AuthenticationEventLogin extends AuthenticationEvent {
  String email;
  String password;
  String firstName;
  String lastName;

  AuthenticationEventLogin({
    required this.email,
    required this.password,
    required this.firstName,
    required this.lastName});
}
