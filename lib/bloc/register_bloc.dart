import 'package:weather_app/events/register_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/functions.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/models/authentication.dart';
import 'package:weather_app/models/register.dart';
import 'package:weather_app/models/user.dart';
import 'package:weather_app/responsitory/user_res.dart';
import 'package:weather_app/services/navigation_services.dart';


class RegisterBloc extends Bloc<RegisterEvent,bool>{
  RegisterBloc() : super(false) {

    on<RegisterEventSubmitting>((event, emit) async {
      if (Authentication.validateEmail(event.email) != null
          || Authentication.validatePassword(event.password) != null
          || Authentication.validateFirstName(event.firstName) != null
          || Authentication.validateLastName(event.lastName) != null
          || Authentication.validatePasswordConfirm(event.password,event.passwordConfirm) != null) {
        emit(true);
      }
      else {
        showProcessDialog();
       Register register = Register(
           fistName: event.firstName,
           lastName: event.lastName,
           email: event.email,
           password: event.password);
       UserResponse? _userResponsitory = await UserResponse.createUser(register);
        injection<NavigationService>().navigatorKey.currentState!.pop();
       if(_userResponsitory != null && _userResponsitory.status!) {
         injection.registerFactory<User>(() => _userResponsitory.user!);
         injection<User>().saveUser();
         injection<NavigationService>().navigatorKey.currentState!.pushNamedAndRemoveUntil('/home',(route) => false);
       }
       else {

         showToastMessage("");
       }
      }
    });

  }
}
