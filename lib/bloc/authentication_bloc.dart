import 'package:weather_app/events/authentication_event.dart';
import 'package:weather_app/functions.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/models/authentication.dart';
import 'package:weather_app/models/user.dart';
import 'package:weather_app/responsitory/user_res.dart';
import 'package:weather_app/services/navigation_services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent,bool> {
  AuthenticationBloc(): super(false) {

    on<AuthenticationEventSubmit>((event, emit) async {
      if(Authentication.validateEmail(event.email) != null
      || Authentication.validatePassword(event.password) != null) {
        emit(true);
      }
      else {
        showProcessDialog();
        Authentication _auth = Authentication(email: event.email, password: event.password);
        try {
          UserResponse? _userResponsitory = await UserResponse.signIn(_auth);
          injection<NavigationService>().navigatorKey.currentState!.pop();
          if(_userResponsitory != null && _userResponsitory.status!) {
            injection.registerFactory<User>(() => _userResponsitory.user!);
            injection<User>().saveUser();
            injection<NavigationService>().navigatorKey.currentState!.pushNamedAndRemoveUntil('/home',(route) => false);
          }
          else {
            showToastMessage("Email already exists");
          }
        } catch (e) {
          showToastMessage(e.toString());
        }
      }
    });
  }


}
