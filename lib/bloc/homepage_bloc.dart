
import 'package:flutter/material.dart';
import 'package:weather_app/functions.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/responsitory/weather_res.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/events/homepage_event.dart';
import 'package:weather_app/models/weather.dart';
import 'package:weather_app/services/navigation_services.dart';
import 'package:weather_app/views/weather_page.dart';

class HomePageBloc extends Bloc<HomePageEvent,List<City>>{
  HomePageBloc(): super (<City>[]) {

    on<HomePageEventLoadingCity>((event, emit) async {
      List<City> cities = await City.loadCityFromLocal();
      emit(cities);
    });

    on<HomePageEventSearching>((event, emit) async {
      WeatherResponse? weatherResponse = await WeatherResponse.getWeatherByCity(event.city.replaceAll(" ", ""));
      if(weatherResponse == null || weatherResponse.getDataValid(weatherResponse.city.timezone).isEmpty) {
        showToastMessage("Không tìm thấy thành phố nào?");
      }
      else {
        injection<NavigationService>().navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (context) => WeatherPage(weatherResponse: weatherResponse))
        );
      }
    });
  }

}
