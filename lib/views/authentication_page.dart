

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/bloc/authentication_bloc.dart';
import 'package:weather_app/events/authentication_event.dart';
import 'package:weather_app/models/authentication.dart';

class AuthenticationPage extends StatefulWidget {
  const AuthenticationPage({Key? key}) : super(key: key);

  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenWith = MediaQuery.of(context).size.width;
    return BlocBuilder<AuthenticationBloc,bool>(
        builder: (context,state) {
        return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: ListView(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(Icons.arrow_back_ios,size: 20,color: Colors.black,),
                      ),
                    ],
                  ),
                  Image.asset(
                    "assets/icons/logo.png",
                    fit: BoxFit.cover,
                    height: 150,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20,left: 20),
                    child: Stack(
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Text("Weather Forecast",style: TextStyle(
                                fontSize: 35,
                                fontWeight: FontWeight.w600,
                                color: Colors.grey[600]
                            ),)),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: 70,
                        width: screenWith - 80,
                        child: TextFormField(
                          controller: _emailController,
                          autovalidate: state,
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.subtitle1,
                          keyboardType: TextInputType.text,
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                            labelText: "Email",
                            labelStyle: Theme.of(context).textTheme.subtitle2,
                            border: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black,),
                            ),
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black,),
                            ),
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black,),
                            ),
                          ),
                          validator: Authentication.validateEmail,
                        ),
                      ),
                      SizedBox(
                        height: 70,
                        width: screenWith - 80,
                        child: TextFormField(
                          autovalidate: state,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          controller: _passwordController,
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.subtitle1,
                          keyboardType: TextInputType.visiblePassword,
                          cursorColor: Colors.black,
                          decoration:  InputDecoration(
                            labelText: "Password",
                            labelStyle: Theme.of(context).textTheme.subtitle2,
                            border: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black,),
                            ),
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black,),
                            ),
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                          ),
                          validator: Authentication.validatePassword,
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 40),
                          child: Container(
                            height: 50,
                            width: screenWith - 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: FloatingActionButton(
                              heroTag: "Login",
                              backgroundColor:Colors.blue[400]!,
                              onPressed: () {
                                context.read<AuthenticationBloc>().add(
                                    AuthenticationEventSubmit(email: _emailController.text,
                                        password: _passwordController.text));
                              }, child: const Text("Login",
                              style:  TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontFamily: "HelveticaNeueBold",
                              ),
                            ),
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
                            ),
                          )),
                    ],
                  )

                ],
              ),
            )
          ],
        )
    );
    });
  }

  void showDialogSubmit() async {
    await Future.delayed(const Duration(seconds: 0));
    showDialog(
        context: context,
        builder: (context) => Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      height: 50,
                      child: Center(
                          child: Platform.isAndroid ? const CircularProgressIndicator(
                            strokeWidth: 1,
                          ):  const CupertinoActivityIndicator(
                            radius: 20,
                          ))),
                ],
              ),
            ),
          ),
        )
    );
  }
}
