
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/bloc/register_bloc.dart';
import 'package:weather_app/events/register_event.dart';
import 'package:weather_app/models/authentication.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    double screenWith = MediaQuery.of(context).size.width;
    return BlocBuilder<RegisterBloc,bool>(builder: (context,state) {
       return Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          body: ListView(
            children: [
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: const Icon(Icons.arrow_back_ios,size: 20,color: Colors.black,),
                        ),
                      ],
                    ),
                    Image.asset(
                      "assets/icons/logo.png",
                      fit: BoxFit.cover,
                      height: 150,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20,left: 20),
                      child: Stack(
                        children: [
                          Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Text("Weather Forecast",style: TextStyle(
                                  fontSize: 35,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey[600]
                              ),)),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Column(
                      children: [
                        SizedBox(
                            height: 70,
                          width: screenWith - 80,
                          child: TextFormField(
                            autovalidate: state,
                            controller: _firstNameController,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.subtitle1,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                                labelText: "First name",
                                labelStyle: Theme.of(context).textTheme.subtitle2,
                                border: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                enabledBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                focusedBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                            ),
                            validator: Authentication.validateFirstName,
                          )
                        ),
                        SizedBox(
                          height: 70,
                          width: screenWith - 80,
                          child: TextFormField(
                            autovalidate: state,
                            controller: _lastNameController,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.subtitle1,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.black,
                            decoration:  InputDecoration(
                              labelText: "Last name",
                              labelStyle: Theme.of(context).textTheme.subtitle2,
                              border: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black,),
                              ),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black,),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),

                            ),
                            validator: Authentication.validateLastName,
                          ),
                        ),
                        SizedBox(
                          height: 70,
                          width: screenWith - 80,
                          child: TextFormField(
                            controller: _emailController,
                            autovalidate: state,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.subtitle1,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                                labelText: "Email",
                                labelStyle: Theme.of(context).textTheme.subtitle2,
                                border: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                enabledBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                focusedBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),

                            ),
                            validator: Authentication.validateEmail,
                          ),
                        ),
                        SizedBox(
                          height: 70,
                          width: screenWith - 80,
                          child: TextFormField(
                            controller: _passwordController,
                            obscureText: true,
                            enableSuggestions: false,
                            autocorrect: false,
                            autovalidate: state,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.subtitle1,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                                labelText: "Password",
                                labelStyle: Theme.of(context).textTheme.subtitle2,
                                border: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                enabledBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),
                                focusedBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black,),
                                ),

                            ),
                            validator: Authentication.validatePassword,
                          ),
                        ),
                        SizedBox(
                          height: 70,
                          width: screenWith - 80,
                          child: TextFormField(
                            controller: _passwordConfirmController,
                            obscureText: true,
                            enableSuggestions: false,
                            autocorrect: false,
                            autovalidate: state,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.subtitle1,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              labelText: "Password confirm",
                              labelStyle: Theme.of(context).textTheme.subtitle2,
                              border: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black,),
                              ),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black,),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black,),
                              ),

                            ),
                            validator: (String? v) => Authentication.validatePasswordConfirm(
                            _passwordController.text, v),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: Container(
                              height: 50,
                              width: screenWith - 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40.0),
                              ),
                              child: FloatingActionButton(
                                heroTag: "signUp",
                                backgroundColor:Colors.blue[200]!,
                                onPressed: () {
                                  context.read<RegisterBloc>().add(
                                      RegisterEventSubmitting(
                                          email: _emailController.text,
                                          password: _passwordController.text,
                                          passwordConfirm: _passwordConfirmController.text,
                                          firstName: _firstNameController.text,
                                          lastName: _lastNameController.text));
                                }, child: const Text("SignUp",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontFamily: "HelveticaNeueBold",
                                ),
                              ),
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(40.0))),
                              ),
                            )),
                      ],
                    )
                  ],
                ),
              )
            ],
          )
      );
    });
  }
}
