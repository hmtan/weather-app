import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/bloc/homepage_bloc.dart';
import 'package:weather_app/events/homepage_event.dart';
import 'package:weather_app/models/weather.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _search = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomePageBloc,List<City>>(builder: (context,state) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: SizedBox(
                  child: Stack(
                    children: const [
                      Align(
                        alignment: Alignment.center,
                        child: Text("Weather",style:  TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600
                        ),),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.list),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child:  Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Row(
                    children: [
                      Flexible(child: TextFormField(
                        controller: _search,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                          fontSize: 13,
                          color: Colors.black54,
                        ),
                        cursorColor: Colors.grey,
                        textAlignVertical: TextAlignVertical.bottom,
                        decoration: InputDecoration(
                          hintText: "Tìm kiếm theo tên thành phố",
                          hintStyle: TextStyle(
                              fontSize: 13,color: Colors.grey[500]!
                          ),
                          prefixIcon: Icon(Icons.search,color: Colors.grey[400]!,),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                        ),
                      ),flex: 8,),
                      Flexible(
                        child: GestureDetector(
                          onTap: () {
                            context.read<HomePageBloc>().add(HomePageEventSearching(city: _search.text));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.blue[200]!,
                                borderRadius: const BorderRadius.only(topRight: Radius.circular(10.0),bottomRight: Radius.circular(10.0))
                            ),
                            child: const Text("Search",style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                            )),
                          ),
                        ),
                        flex: 2,)
                    ],
                  ),
                ),
              ),
              // Expanded(child: )
            ],
          ),
        ),
      );
    });
  }
}
