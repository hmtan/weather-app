
import 'package:weather_app/models/weather.dart';
import 'package:weather_app/responsitory/weather_res.dart';
import 'package:flutter/material.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({Key? key, required this.weatherResponse}) : super(key: key);

  final WeatherResponse weatherResponse;

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    int _timeZone = weatherResponse.city.timezone;
    WeatherData _weatherData = weatherResponse.getDataValid(_timeZone)[0];
    return Scaffold(
      body: Hero(
        tag: weatherResponse.city.name,
        child: Stack(
          children: [
            SizedBox(
              height: h,
              width: w,
              child: Image.asset(_weatherData.getPathImage(_timeZone),fit: BoxFit.fill),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 60),
              child: ListView(
                children: [
                  Center(child: Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(weatherResponse.city.name,style: const TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w600
                        ),),
                        Text(" " + _weatherData.weather.getTemp.toString() + "°",style: const TextStyle(
                            fontSize: 80,
                            color: Colors.white,
                            fontWeight: FontWeight.w200
                        ),),
                        Text(_weatherData.weatherDes[0].getDescription.toString(),style: const TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w500
                        ),),
                       const SizedBox(
                          height: 5,
                        ),
                        Text(_weatherData.weather.getTempDes,style: const TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.w500
                        ),),
                      ],
                    ),
                  ))
                ],
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20,60,20,0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: const Icon(Icons.arrow_back_ios,size: 20,color: Colors.white,),
                    ),
                    const Text("Thêm",style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600
                    ),)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
