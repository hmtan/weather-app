

import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
      body: ListView(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Image.asset(
                  "assets/icons/logo.png",
                  fit: BoxFit.cover,
                  height: 150,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10,left: 20),
                  child: Stack(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Text("Weather Forecast",style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[600]
                          ),)),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10,left: 20),
                  child: Stack(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Text("WelCome",style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[600]
                          ),)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: w/2 - 20,
                      height: h/2 - 30,
                      child: FloatingActionButton(
                        backgroundColor: Colors.blue[400]!,
                        heroTag: "Login",
                        onPressed: () {
                          Navigator.of(context).pushNamed("/Login");
                        },
                        child: const Text("Login"),
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20))),
                      ),
                    ),
                    SizedBox(
                      width: w/2 - 20,
                      height: h/2 - 80,
                      child: FloatingActionButton(
                        backgroundColor: Colors.blue[200]!,
                        heroTag: "signUp",
                        onPressed: () {
                          Navigator.of(context).pushNamed("/signUp");
                        },
                        child: const Text("SignUp"),
                        shape:  const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), bottomLeft: Radius.circular(20))),
                      ),
                    )

                  ],
                )
              ],
            ),
          )
        ],
      )
    );
  }
}
