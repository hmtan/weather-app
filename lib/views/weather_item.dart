
import 'package:flutter/cupertino.dart';
import 'package:weather_app/models/weather.dart';

class WeatherItem extends StatelessWidget {
  const WeatherItem({Key? key, required this.weather}) : super(key: key);

  final Weather weather;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0)
      ),
    );
  }
}
