
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';
import 'package:weather_app/bloc/authentication_bloc.dart';
import 'package:weather_app/bloc/homepage_bloc.dart';
import 'package:weather_app/injection.config.dart';
import 'package:weather_app/responsitory/network.dart';
import 'package:weather_app/services/navigation_services.dart';
import 'bloc/register_bloc.dart';

final injection = GetIt.instance;

Future setup() async {
  await configureDependencies();
  injection.registerFactory<RegisterBloc>(() => RegisterBloc());
  injection.registerFactory<AuthenticationBloc>(() => AuthenticationBloc());
  injection.registerLazySingleton<NavigationService>(() => NavigationService());
  injection.registerFactoryAsync<Database>(() async {
    return await openDatabase("city.db",version: 1,onCreate: (db,version) async {
      await db.execute(
          'CREATE TABLE City (id INTEGER PRIMARY KEY, name TEXT, country TEXT, email TEXT)');
    });
  });
  injection.registerFactory<HomePageBloc>(() => HomePageBloc());
  injection.registerSingleton<Dio>(Dio());
  injection.registerSingleton<RestClient>(RestClient(injection<Dio>()));
  injection.registerSingleton<RestWeather>(RestWeather(injection<Dio>()));
}


@InjectableInit()
Future<void> configureDependencies() async =>  $initGetIt(injection);





