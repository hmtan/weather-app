
import 'package:json_annotation/json_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/models/authentication.dart';
import 'package:weather_app/models/register.dart';
import 'package:weather_app/models/user.dart';
import 'package:dio/dio.dart';
import 'package:weather_app/responsitory/network.dart';
part 'user_res.g.dart';

@JsonSerializable()
class UserResponse {
  bool? status;
  String? message;
  @JsonKey(name: "data")
  User? user;

  UserResponse({this.status,this.message,this.user});

  factory UserResponse.fromJson(Map<String,dynamic> json) => _$UserResponseFromJson(json);

  Map<String,dynamic> toJson() => _$UserResponseToJson(this);

  static Future<User> loadUserFromLocal() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    User _user = User();
    _user.email = prefs.getString("email") ?? "";
    _user.password = prefs.getString("password") ?? "";
    _user.token = prefs.getString("token") ?? "";
    _user.firstName = prefs.getString("firstName") ?? "";
    _user.lastName = prefs.getString("lastName") ?? "";
    return _user;
  }

  static Future<UserResponse?> createUser(Register register) async {
    try {
      return await injection<RestClient>().userRegister(register);
    } catch (e) {
      return null;
    }
  }

  static Future<UserResponse?> signIn(Authentication authentication) async {
    try {
      return await injection<RestClient>().login(authentication);
    } catch (e) {
      return null;
    }
  }

}
