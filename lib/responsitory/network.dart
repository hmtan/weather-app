import 'package:weather_app/constants.dart';

import 'weather_res.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:weather_app/models/authentication.dart';
import 'package:weather_app/models/register.dart';
import 'package:weather_app/responsitory/user_res.dart';
part 'network.g.dart';

@RestApi(baseUrl: "http://52.221.219.28:8080/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("api/auth/login")
  Future<UserResponse> login(@Body() Authentication authentication);

  @POST("api/auth/register")
  Future<UserResponse> userRegister(@Body() Register register);

}

@RestApi(baseUrl: "https://community-open-weather-map.p.rapidapi.com/")
abstract class RestWeather {

  factory RestWeather(Dio dio, {String baseUrl}) = _RestWeather;

  @GET("forecast")
  Future<WeatherResponse> getWeatherByCity(@Header("x-rapidapi-key") String apiKey,@Query("q") String city);

}
