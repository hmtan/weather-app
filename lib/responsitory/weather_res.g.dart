// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherResponse _$WeatherResponseFromJson(Map<String, dynamic> json) {
  return WeatherResponse(
    city: City.fromJson(json['city'] as Map<String, dynamic>),
    data: (json['list'] as List<dynamic>)
        .map((e) => WeatherData.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$WeatherResponseToJson(WeatherResponse instance) =>
    <String, dynamic>{
      'city': instance.city,
      'list': instance.data,
    };
