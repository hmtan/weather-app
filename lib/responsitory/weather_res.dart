import 'package:json_annotation/json_annotation.dart';
import 'package:weather_app/constants.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/models/weather.dart';
import 'package:weather_app/responsitory/network.dart';
part 'weather_res.g.dart';

@JsonSerializable()
class WeatherResponse {
  @JsonKey(name: "city")
  City city;
  @JsonKey(name: "list")
  List<WeatherData> data;


  WeatherResponse({required this.city, required this.data});

  factory WeatherResponse.fromJson(Map<String,dynamic> json) => _$WeatherResponseFromJson(json);

  Map<String,dynamic> toJson() => _$WeatherResponseToJson(this);

  static Future<WeatherResponse?> getWeatherByCity(String city) async {
    try {
      return await injection<RestWeather>().getWeatherByCity(Constants.apiKey, city);
    } catch (e) {
      return null;
    }
  }

  List<WeatherData> getDataValid(int timeZone) {
    List<WeatherData> _list = <WeatherData>[];
    for (WeatherData e in data) {
      DateTime time = e.getTime;
      if(time.isAfter(DateTime.now().add(Duration(seconds: timeZone - 25200)))
          || time.add(const Duration(hours: 2)).isAfter(DateTime.now())) {
        _list.add(e);
      }
    }
    return _list;
  }

  List<WeatherData> getDataFuture(int timeZone) {
    List<WeatherData> _list = <WeatherData>[];
    DateTime time = DateTime.now().add(Duration(seconds: timeZone - 25200));
    for (WeatherData e in data) {
      if(e.getTime.day > time.day) {
        time.add(const Duration(days: 1));
        _list.add(e);
      }
    }
    return _list;
  }

  List<WeatherData> getDataInDay(int timeZone) {
    List<WeatherData> _list = <WeatherData>[];
    for (WeatherData e in getDataValid(timeZone)) {
      DateTime time = e.getTime;
      if(time.day == DateTime.now().add(Duration(seconds: timeZone - 25200)).day) {
        _list.add(e);
      }
    }
    return _list;
  }
}
