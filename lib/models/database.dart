

import 'package:sqflite/sqflite.dart';
import 'package:weather_app/models/weather.dart';

class DatabaseCity{
  Database db;
  DatabaseCity({required this.db});

  static Future<Database> startDb(String name) async {
    return await openDatabase(name,version: 1,onCreate: (db,version) async {
      await db.execute(
          'CREATE TABLE City (id INTEGER PRIMARY KEY, name TEXT, country TEXT)');
    });
  }

  Future<City?> insert(City city) async {
    try {
      await db.transaction((txn) async {
        await txn.rawInsert(
            'INSERT INTO City(name, country) VALUES("${city.name}", "${city.country}")');
      });
      return city;
    } catch (e) {
      return null;
    }
  }

  Future<List<City>> select() async {
    String query = 'SELECT * FROM City';
    try {
      List data = await db.rawQuery(query);
      return List<City>.from(data.map((e) => City.fromJson(e))).toList();
    } catch (e) {
      return <City>[];
    }
  }



}
