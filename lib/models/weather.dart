import 'package:json_annotation/json_annotation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/models/user.dart';
part 'weather.g.dart';

@JsonSerializable()
class WeatherDescription {
  @JsonKey(name: "main")
  String title;

  WeatherDescription({required this.title});

  factory WeatherDescription.fromJson(Map<String,dynamic> json) => _$WeatherDescriptionFromJson(json);

  Map<String,dynamic> toJson() => _$WeatherDescriptionToJson(this);


  String get getDescription{
    if(title == "Clouds") {
      return "Có Nắng";
    }
    else if(title == "Rain") {
      return "Có mưa";
    }
    else if (title == "Clear") {
      return "Thông thoáng";
    }
    return "";
  }
}


@JsonSerializable()
class Weather {
  int id;
  double humidity;
  double pressure;
  double temp;
  @JsonKey(name: "temp_min")
  double tempMin;
  @JsonKey(name: "temp_max")
  double tempMax;
  @JsonKey(name: "temp_kf")
  double tempKf;
  @JsonKey(name: "feels_like")
  double feelsLike;


  Weather({required this.id,
    required this.humidity,
    required this.pressure,
    required this.tempKf,
    required this.tempMax,
    required this.tempMin,
    required this.temp,
    required this.feelsLike
  });

  factory Weather.fromJson(Map<String,dynamic> json) => _$WeatherFromJson(json);

  Map<String,dynamic> toJson() => _$WeatherToJson(this);

  int get getTemp{
    return temp~/10;
  }

  int get getTempKf{
    return tempKf~/10;
  }

  String get getTempDes{
    return "C:$getTemp°" " T:$getTempKf°";
  }

}

@JsonSerializable()
class City {
  int id;
  String name;
  String country;
  int timezone;

  City({required this.id, required this.name, required this.country, required this.timezone});

  factory City.fromJson(Map<String,dynamic> json) => _$CityFromJson(json);

  Map<String,dynamic> toJson() => _$CityToJson(this);

  static Future<List<City>> loadCityFromLocal() async {
    String query = 'SELECT * FROM City WHERE email = ${injection<User>().email}';
    try {
      List data = await injection<Database>().rawQuery(query);
      return List<City>.from(data.map((e) => City.fromJson(e))).toList();
    } catch (e) {
      return <City>[];
    }
  }


}

@JsonSerializable()
class Wind {
  double speed;

  Wind({required this.speed});

  factory Wind.fromJson(Map<String,dynamic> json) => _$WindFromJson(json);

  Map<String,dynamic> toJson() => _$WindToJson(this);
}

@JsonSerializable()
class WeatherData {
  @JsonKey(name: "main")
  Weather weather;
  @JsonKey(name: "weather")
  List<WeatherDescription> weatherDes;
  Wind wind;
  int visibility;
  @JsonKey(name: "dt_txt")
  String dt;

  WeatherData({required this.weather,
    required this.weatherDes,
    required this.wind,
    required this.visibility,
    required this.dt
  });

  factory WeatherData.fromJson(Map<String,dynamic> json) => _$WeatherDataFromJson(json);

  Map<String,dynamic> toJson() => _$WeatherDataToJson(this);

  String getPathImage(int timeZone) {
    if(weatherDes[0].title == "Rain") {
      return "assets/images/rain.png";
    }
    else {
      int hour = getTime.hour;
      if (hour >= 0 && hour <= 5) {
        return "assets/images/night.png";
      }
      else if (hour > 5 && hour <= 17 && weatherDes[0].title == "clear") {
        return "assets/images/clear.png";
      }
      else if (hour > 5 && hour <= 17) {
         return "assets/images/sunny.png";
      }
      else if (hour > 17 && hour <= 20) {
        return "assets/images/noon.png";
      }
      else {
        return "assets/images/afternoon.png";
      }
    }
  }

  DateTime get getTime {
    return DateTime.parse(dt);
  }

}




