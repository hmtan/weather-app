
import 'package:json_annotation/json_annotation.dart';
part 'authentication.g.dart';

@JsonSerializable()
class Authentication {
  String email;
  String password;

  Authentication({required this.email, required this.password});

  factory Authentication.fromJson(Map<String,dynamic> json) => _$AuthenticationFromJson(json);

  Map<String, dynamic> toJson() => _$AuthenticationToJson(this);


  static String? validateEmail(String? value) {
    final RegExp emailExp = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (!emailExp.hasMatch(value!)) return "Email không đúng định dạng";
    return null;
  }

  static String? validatePassword(String? password) {
    if(password!.trim().isNotEmpty) {
      return null;
    }
    return "Vui lòng nhập mật khẩu";
  }

  static String? validatePasswordConfirm(String? password,String? passwordConfirm) {
    if(passwordConfirm != password) {
      return "Mật khẩu không khớp";
    }
    return null;
  }

  static String? validateFirstName(String? name) {
    if(name!.trim().isNotEmpty) {
      return null;
    }
    return "Vui lòng nhập họ";
  }

  static String? validateLastName(String? name) {
    if(name!.trim().isNotEmpty) {
      return null;
    }
    return "Vui lòng nhập tên";
  }


}
