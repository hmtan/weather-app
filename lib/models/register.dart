
import 'package:json_annotation/json_annotation.dart';
part 'register.g.dart';

@JsonSerializable()
class Register {
  @JsonKey(name: "first_name")
  String fistName;
  @JsonKey(name: "last_name")
  String lastName;
  String email;
  String password;

  Register({
    required this.fistName,
    required this.lastName,
    required this.email,
    required this.password});

  factory Register.fromJson(Map<String,dynamic> json) => _$RegisterFromJson(json);

  Map<String,dynamic> toJson() => _$RegisterToJson(this);

}


