
import 'package:json_annotation/json_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(name: "first_name")
  String? firstName;
  @JsonKey(name: "last_name")
  String? lastName;
  @JsonKey(name: "email")
  String? email;
  String? password;
  String? token;

  User({this.firstName,this.lastName,this.email,this.token,this.password});

  factory User.fromJson(Map<String,dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  void saveUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("firstName", firstName!);
    prefs.setString("lastName", lastName!);
    prefs.setString("email", email!);
    prefs.setString("password", password!);
    prefs.setString("token", token!);
  }

}
