// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherDescription _$WeatherDescriptionFromJson(Map<String, dynamic> json) {
  return WeatherDescription(
    title: json['main'] as String,
  );
}

Map<String, dynamic> _$WeatherDescriptionToJson(WeatherDescription instance) =>
    <String, dynamic>{
      'main': instance.title,
    };

Weather _$WeatherFromJson(Map<String, dynamic> json) {
  return Weather(
    id: json['id'] as int,
    humidity: (json['humidity'] as num).toDouble(),
    pressure: (json['pressure'] as num).toDouble(),
    tempKf: (json['temp_kf'] as num).toDouble(),
    tempMax: (json['temp_max'] as num).toDouble(),
    tempMin: (json['temp_min'] as num).toDouble(),
    temp: (json['temp'] as num).toDouble(),
    feelsLike: (json['feels_like'] as num).toDouble(),
  );
}

Map<String, dynamic> _$WeatherToJson(Weather instance) => <String, dynamic>{
      'id': instance.id,
      'humidity': instance.humidity,
      'pressure': instance.pressure,
      'temp': instance.temp,
      'temp_min': instance.tempMin,
      'temp_max': instance.tempMax,
      'temp_kf': instance.tempKf,
      'feels_like': instance.feelsLike,
    };

City _$CityFromJson(Map<String, dynamic> json) {
  return City(
    id: json['id'] as int,
    name: json['name'] as String,
    country: json['country'] as String,
    timezone: json['timezone'] as int,
  );
}

Map<String, dynamic> _$CityToJson(City instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'country': instance.country,
      'timezone': instance.timezone,
    };

Wind _$WindFromJson(Map<String, dynamic> json) {
  return Wind(
    speed: (json['speed'] as num).toDouble(),
  );
}

Map<String, dynamic> _$WindToJson(Wind instance) => <String, dynamic>{
      'speed': instance.speed,
    };

WeatherData _$WeatherDataFromJson(Map<String, dynamic> json) {
  return WeatherData(
    weather: Weather.fromJson(json['main'] as Map<String, dynamic>),
    weatherDes: (json['weather'] as List<dynamic>)
        .map((e) => WeatherDescription.fromJson(e as Map<String, dynamic>))
        .toList(),
    wind: Wind.fromJson(json['wind'] as Map<String, dynamic>),
    visibility: json['visibility'] as int,
    dt: json['dt_txt'] as String,
  );
}

Map<String, dynamic> _$WeatherDataToJson(WeatherData instance) =>
    <String, dynamic>{
      'main': instance.weather,
      'weather': instance.weatherDes,
      'wind': instance.wind,
      'visibility': instance.visibility,
      'dt_txt': instance.dt,
    };
