import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:weather_app/injection.dart';
import 'package:weather_app/services/navigation_services.dart';

void showProcessDialog() {
  showDialog(
      context: injection<NavigationService>().navigatorKey.currentContext!,
      builder: (context) => Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                    height: 50,
                    child: Center(
                        child: Platform.isAndroid ? const CircularProgressIndicator(
                          strokeWidth: 1,
                        ):  const CupertinoActivityIndicator(
                          radius: 20,
                        ))),
              ],
            ),
          ),
        ),
      )
  );
}

void showToastMessage(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black38,
      textColor: Colors.white,
      fontSize: 16.0
  );
}
